# Picobus
An 8p PocketQube deployer

## Folders
**docs**: RST Documentation  
**Documentation**: ODT Documents  
**mech**: Mechanical parts  
**picobus-deployer-pcb**: PicoBus Controller  

## Documents
[View latest generated docs](https://librespacefoundation.gitlab.io/picobus/)

## Deployer PCB
For this kicad project you will need the [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) library.

## License

Licensed under the [CERN OHLv1.2](LICENSE) 2019 Libre Space Foundation.
