.. PicoBus documentation master file, created by
   sphinx-quickstart on Tue Jul  2 16:39:43 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PicoBus's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   controller



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
